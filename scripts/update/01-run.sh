#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Umbrel. https://getumbrel.com
# SPDX-FileCopyrightText: 2021-2024 Citadel and contributors
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -euo pipefail

RELEASE=$1
CITADEL_ROOT=$2

# Only used on Citadel OS
SD_CARD_CITADEL_ROOT="/sd-root${CITADEL_ROOT}"

echo
echo "======================================="
echo "=============== UPDATE ================"
echo "======================================="
echo "=========== Stage: Install ============"
echo "======================================="
echo

[[ -f "/etc/default/citadel" ]] && source "/etc/default/citadel"

# Make Citadel OS specific updates
if [[ ! -z "${CITADEL_OS:-}" ]] || [[ ! -z "${CITADEL_OS_CUBIT:-}" ]]; then
    if [[ ! -z "${CITADEL_OS_CUBIT:-}" ]]; then
        SD_CARD_CITADEL_ROOT="/main-root${CITADEL_ROOT}"
        CITADEL_OS="$CITADEL_OS_CUBIT"
    fi
    echo
    echo "============================================="
    echo "Installing on Citadel OS $CITADEL_OS"
    echo "============================================="
    echo
    
    # Update SD card installation
    if  [[ -f "${SD_CARD_CITADEL_ROOT}/.citadel" ]]; then
        echo "Replacing ${SD_CARD_CITADEL_ROOT} on SD card with the new release"
        rsync --archive \
            --verbose \
            --include-from="${CITADEL_ROOT}/.citadel-${RELEASE}/scripts/update/.updateinclude" \
            --exclude-from="${CITADEL_ROOT}/.citadel-${RELEASE}/scripts/update/.updateignore" \
            --delete \
            "${CITADEL_ROOT}/.citadel-${RELEASE}/" \
            "${SD_CARD_CITADEL_ROOT}/"

        echo "Fixing permissions"
        chown -R 1000:1000 "${SD_CARD_CITADEL_ROOT}/"
    else
        echo "ERROR: No Citadel installation found at SD root ${SD_CARD_CITADEL_ROOT}"
        echo "Skipping updating on SD Card..."
    fi

    # This makes sure systemd services are always updated (and new ones are enabled).
    CITADEL_SYSTEMD_SERVICES="${CITADEL_ROOT}/.citadel-${RELEASE}/scripts/citadel-os/services/*.service"
    if [[ ! -z "${CITADEL_OS_CUBIT:-}" ]]; then
        CITADEL_SYSTEMD_SERVICES="${CITADEL_ROOT}/.citadel-${RELEASE}/scripts/citadel-os/cubit/services/*.service"
    fi
    for service_path in $CITADEL_SYSTEMD_SERVICES; do
      service_name=$(basename "${service_path}")
      install -m 644 "${service_path}" "/etc/systemd/system/${service_name}"
      systemctl enable "${service_name}"
    done
fi

cd "$CITADEL_ROOT"

# Stopping karen
echo "Stopping background daemon"
cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "installing", "progress": 55, "description": "Stopping background daemon", "updateTo": "$RELEASE"}
EOF
pkill -f "\./karen" || true


# Installing k3s
echo "Installing Nirvati components"
cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "installing", "progress": 60, "description": "Installing Nirvati components", "updateTo": "$RELEASE"}
EOF
mkdir -p /var/lib/rancher/k3s/server/manifests/
mkdir -p /var/lib/rancher/k3s/agent/images/
mkdir -p /etc/rancher/k3s/
curl https://get.nirvati.org/registries.yaml -o /etc/rancher/k3s/registries.yaml
curl https://get.nirvati.org/cilium.yml -o /var/lib/rancher/k3s/server/manifests/cilium.yml
curl https://get.nirvati.org/nirvati-dns.yml -o /var/lib/rancher/k3s/server/manifests/nirvati-dns.yml
if [ -f /run/systemd/resolve/resolv.conf ]; then
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--resolv-conf /run/systemd/resolve/resolv.conf --disable=metrics-server,local-storage,coredns,servicelb,traefik --flannel-backend=none --disable-network-policy --disable-kube-proxy" sh -s - --docker
else
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable=metrics-server,local-storage,coredns,servicelb,traefik --flannel-backend=none --disable-network-policy --disable-kube-proxy" sh -s - --docker
fi

echo "Stopping installed apps"
cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "installing", "progress": 70, "description": "Stopping installed apps", "updateTo": "$RELEASE"}
EOF
./scripts/app stop installed || true

# Stop old containers
echo "Stopping old containers"
cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "installing", "progress": 80, "description": "Stopping old containers", "updateTo": "$RELEASE"}
EOF
./scripts/stop || true

# Overlay home dir structure with new dir tree
echo "Overlaying $CITADEL_ROOT/ with new directory tree"
rsync --archive \
    --verbose \
    --include-from="$CITADEL_ROOT/.citadel-$RELEASE/scripts/update/.updateinclude" \
    --exclude-from="$CITADEL_ROOT/.citadel-$RELEASE/scripts/update/.updateignore" \
    --delete \
    "$CITADEL_ROOT"/.citadel-"$RELEASE"/ \
    "$CITADEL_ROOT"/

# Fix permissions
echo "Fixing permissions"
find "$CITADEL_ROOT" -path "$CITADEL_ROOT/app-data" -prune -o -exec chown 1000:1000 {} + || true
chmod -R 700 "$CITADEL_ROOT"/tor/data/* || true

cd "$CITADEL_ROOT"
 
# Start updated containers
echo "Starting new containers"
cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "installing", "progress": 90, "description": "Starting new containers", "updateTo": "$RELEASE"}
EOF
./scripts/start || true


cat <<EOF > "$CITADEL_ROOT"/statuses/update-status.json
{"state": "success", "progress": 100, "description": "Successfully installed Citadel $RELEASE", "updateTo": ""}
EOF

# Make Citadel OS specific post-update changes
if [[ ! -z "${CITADEL_OS:-}" ]]; then
  # Delete unused Docker images on Citadel OS
  echo "Deleting previous images"
  docker image prune --all --force
fi
