#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Umbrel. https://getumbrel.com
# SPDX-FileCopyrightText: 2021-2022 Citadel and contributors
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -euo pipefail

RELEASE=$1
CITADEL_ROOT=$2

echo
echo "======================================="
echo "=============== UPDATE ================"
echo "======================================="
echo "========= Stage: Post-update =========="
echo "======================================="
echo

cp -f $CITADEL_ROOT/apps/lnd/lnd.conf $CITADEL_ROOT/app-data/lnd/lnd.conf
cp -f $CITADEL_ROOT/apps/lnd/torrc $CITADEL_ROOT/app-data/lnd/torrc
sudo docker restart lnd-service-1 lnd-tor-1

# Nothing here for now
